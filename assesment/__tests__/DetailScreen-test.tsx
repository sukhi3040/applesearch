import React from 'react';
import renderer from 'react-test-renderer';
import DetailScreen from '../src/screens/Details/DetailScreen'

it('Snapshot Listing Screen', () => {
    let tree = renderer.create(<DetailScreen/>).toJSON()
    expect(tree).toMatchSnapshot()
})