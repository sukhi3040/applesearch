import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import renderer from 'react-test-renderer';
import ListScreen from '../src/screens/Dashboard/ListScreen'

it('Snapshot Listing Screen', () => {
    let tree = renderer.create(<ListScreen/>).toJSON()
    expect(tree).toMatchSnapshot()
})

it('checks if Async Storage is used', async () => {
    let listInstance = renderer.create(<ListScreen/>).getInstance()
    expect(AsyncStorage.getItem).toBeCalledWith('cachedResponse');
  })