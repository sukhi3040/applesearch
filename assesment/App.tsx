/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler'
import React from 'react';
import AppNavigator, { } from './src/navigation/AppNavigator'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import AppleSearchReducer from './src/reducer/AppleSearchReducer'


import {
  StatusBar,
} from 'react-native';

const appStore = createStore(AppleSearchReducer)

const App = () => {
  return (
    <Provider store={appStore}>
      <>
        <StatusBar barStyle="dark-content" />
        <AppNavigator />
      </>
    </Provider>
  );
};

export default App;
