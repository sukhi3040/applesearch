export const AppName = 'iTunes Search'

export const stylingParams = {
    primaryColor: 'black',
    secondaryColor: '#f3f3f3'
}