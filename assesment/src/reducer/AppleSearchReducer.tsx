import { combineReducers } from 'redux';
import ArtworkListingReducer from '../screens/Dashboard/ListingReducer'

export default combineReducers({
    albumListing: ArtworkListingReducer
})