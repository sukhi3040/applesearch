import { StyleSheet } from 'react-native'
import { stylingParams } from '../../constants/AppConfiguration'

const styles = StyleSheet.create({
    background: {
        height: '100%',
        width: '100%',
        backgroundColor: stylingParams.secondaryColor
    },
    albumImageStyle: {
        height:150,
        width:150,
        alignSelf:'center',
        margin:20
    },
    headingStyle:{
        fontSize:14.0,
        fontWeight:'bold'
    },
    normalTextStyle:{
        fontSize:13.0,
        fontWeight:'normal'
    },
    flatlistStyle:{
        marginHorizontal:20,
        flex:1
    },
    rowStyle:{
    marginVertical:3
    },
    playPreview:{
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'center',
        height:40,
        width:150,
        margin:30,
        backgroundColor:'gray',
        borderRadius:20,
        position:'absolute',
        bottom:20
    },
    videoPlayerStyle:{
        height:'100%',
        width:'100%',
        position:'absolute',
        backgroundColor:'black'

    }
})

export default styles