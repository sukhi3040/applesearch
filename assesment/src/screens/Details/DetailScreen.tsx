import React from 'react'
import { View, FlatList, ListRenderItemInfo, Text, TouchableOpacity, Alert } from 'react-native'
import FastImage from 'react-native-fast-image'
import AlbumModel from '../../api/models/AlbumArtworkModel'
import NetInfo from '@react-native-community/netinfo'
import styles from './DetailStyles'
import Video from 'react-native-video'
import { AppName } from '../../constants/AppConfiguration'


interface AlbumDetailstate {
    videoPath: string,
    additionalData: any
    artworkUrl: string
    isPlaying: boolean
}

interface ArtworkDetailProps {
    navigation?: any,
    route?: any
}


class ArtworkDetail extends React.Component<ArtworkDetailProps, AlbumDetailstate>{
    constructor(props: ArtworkDetailProps) {
        super(props)
        let artwork = this.props.route.params.selectedArtwork as AlbumModel
        this.state = {
            videoPath: (artwork.previewUrl != null && artwork.previewUrl.length > 0) ? artwork.previewUrl : '',
            artworkUrl: artwork.artworkUrl100,
            additionalData: this.extractDataToDisplay(artwork),
            isPlaying: false
        }
    }

    render() {
        return <View style={styles.background}>
            {this.state.artworkUrl ? <FastImage style={styles.albumImageStyle} source={{ uri: this.state.artworkUrl }} resizeMode={'contain'} /> : null}
            {this.state.additionalData ? <FlatList
                data={Object.keys(this.state.additionalData)}
                keyExtractor={(_item, index) => index.toString()}
                renderItem={this.renderDetail}
                style={styles.flatlistStyle}
                showsVerticalScrollIndicator={false}
            /> : null}
            {this.state.isPlaying ? <Video source={{ uri: this.state.videoPath }} style={styles.videoPlayerStyle} onEnd={this.playVideoPreview} playInBackground={true} fullscreen={true} controls={true} /> : null}
            {this.state.videoPath.length > 0 ? <TouchableOpacity style={styles.playPreview} onPress={this.playVideoPreview}>
                <Text style={[styles.headingStyle, { color: 'white' }]}>{this.state.isPlaying ? 'Stop Preview' : 'Play Preview'}</Text>
            </TouchableOpacity> : null}
        </View>
    }

    renderDetail = (obj: ListRenderItemInfo<string>) => {
        return (
            <View style={styles.rowStyle}>
                <Text style={styles.headingStyle}>{obj.item}</Text>
                <Text style={styles.normalTextStyle}>{this.state.additionalData![obj.item]}</Text>
            </View>
        )
    }

    playVideoPreview = () => {
        NetInfo.fetch().then((netState) => {
            if (netState.isConnected) {
                this.setState({
                    isPlaying: !this.state.isPlaying
                })
            }
            else{
                Alert.alert(AppName, 'Cannot play preview in offline mode.',[{text:'Ok'}])
            }
        })
    }

    extractDataToDisplay = (artwork: AlbumModel) => {
        var dataToDisplay: any = {}
        if (artwork.collectionArtistId) {
            dataToDisplay['Artist Id'] = artwork.collectionArtistId
        }

        if (artwork.artistName) {
            dataToDisplay['Artist Name'] = artwork.artistName
        }

        if (artwork.collectionName) {
            dataToDisplay['Collection Name'] = artwork.collectionName
        }

        if (artwork.trackName) {
            dataToDisplay['Track Name'] = artwork.trackName
        }

        if (artwork.country) {
            dataToDisplay['Country'] = artwork.country
        }

        if (artwork.longDescription) {
            dataToDisplay['Description'] = artwork.longDescription
        }
        return dataToDisplay
    }

}

export default ArtworkDetail