import React from 'react'
import { View, FlatList, TouchableOpacity, ListRenderItemInfo } from 'react-native'
import FastImage from 'react-native-fast-image'
import APIManager from '../../api/APIManager'
import AlbumModel from '../../api/models/AlbumArtworkModel'
import ListingStyle from './ListingStyles'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { UPDATE_ARTWORKS } from './ListingActions'
import NetInfo from '@react-native-community/netinfo'
import AsyncStorage from '@react-native-community/async-storage'
import messaging from '@react-native-firebase/messaging'

interface ListingScreenState {

}

interface ListingScreenProps {
    UPDATE_ARTWORKS: (artworks: AlbumModel[]) => any
    artworkAlbums: AlbumModel[]
    navigation?: any
}


class ListingScreen extends React.Component<ListingScreenProps, ListingScreenState>{
    constructor(props: ListingScreenProps) {
        super(props)

    }

    render() {
        return (
            <View style={ListingStyle.background}>
                <FlatList
                    data={this.props.artworkAlbums}
                    contentContainerStyle={ListingStyle.gridStyle}
                    keyExtractor={(_item, index) => index.toString()}
                    renderItem={this.renderGridItem}
                    style={ListingStyle.flatlistStyle}
                    numColumns={3}
                />
            </View>
        )
    }

    componentDidMount = () => {
        this.getAllAlbums()
        this.requestUserPermission()
    }

    // Flatlist Rendering and Selection
    renderGridItem = (obj: ListRenderItemInfo<AlbumModel>) => {
        return <TouchableOpacity style={ListingStyle.gridItemStyle} onPress={() => this.onSelectGridItem(obj.item)}>
            <FastImage style={{ flex: 1 }} source={{ uri: obj.item.artworkUrl100 }} resizeMode={'contain'} />
        </TouchableOpacity>
    }

    onSelectGridItem = (item: AlbumModel) => {
        this.props.navigation.navigate('Details', { selectedArtwork: item })

    }

    //Server call for getting items
    getAllAlbums = () => {
        NetInfo.fetch().then((netState) => {
            if (netState.isConnected === true) {
                APIManager.getRequest('jack jones', (response: any) => {
                    console.log(response)
                    this.props.UPDATE_ARTWORKS(response.results as AlbumModel[])
                    //can use redux-persist also.
                    AsyncStorage.setItem('cachedResponse', JSON.stringify(response))
                })
            }
            else {
                AsyncStorage.getItem('cachedResponse').then((str) => {
                    if (str) {
                        let obj = JSON.parse(str)
                        this.props.UPDATE_ARTWORKS(obj.results as AlbumModel[])
                    }
                })
            }
        })
    }

    requestUserPermission = async () => {
        const authStatus = await messaging().requestPermission();
        const enabled =
            authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
            authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        if (enabled === true) {
            messaging().getToken().then((token) => {
            }).catch((e) => {
                console.log(e)
            })
        }
    }
}



const mapStateToProps = (state: any) => {
    return (
        {
            artworkAlbums: state.albumListing.albums
        }
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return (bindActionCreators({ UPDATE_ARTWORKS }, dispatch))
}

export default connect(mapStateToProps, mapDispatchToProps)(ListingScreen)