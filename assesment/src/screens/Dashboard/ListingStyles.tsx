import { Dimensions, StyleSheet } from 'react-native'
import { stylingParams } from '../../constants/AppConfiguration'

const styles = StyleSheet.create({
    background: {
        height: '100%',
        width: '100%',
        backgroundColor: stylingParams.secondaryColor
    },
    gridStyle: {
        justifyContent: 'space-evenly'
    },
    gridItemStyle: {
        width: Dimensions.get('screen').width / 3.0 - 10,
        height: undefined,
        aspectRatio: 1,
        margin: 5
    },
    flatlistStyle: {
        flex: 1,
    }
})

export default styles