const INITIAL_STATE = {
    albums: []
};

const artworkListingReducer = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {

        case 'ADD_ALBUMS':
            {
                return { ...state, albums: action.payload }
            }
        default:
            return state
    }
};

export default artworkListingReducer