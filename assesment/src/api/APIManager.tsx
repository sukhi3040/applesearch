
export const apiBaseURL = 'https://itunes.apple.com/search?term='


class APIManager{

    static getRequest = (url: string, callbabk: (resp: any) => void) => {
        let headers = {
            'Accept': 'application/json',
            'content-type': 'application/json'
        }

        fetch(apiBaseURL + url, {
            method: 'GET', headers: headers
        }).then((response) => response.json()).then((json) => callbabk(json)).catch((error) => {
            console.log(error)
        })
    }
}

export default APIManager
