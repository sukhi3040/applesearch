import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import ListScreen from '../screens/Dashboard/ListScreen'
import AlbumDetails from '../screens/Details/DetailScreen'
import { AppName, stylingParams } from '../constants/AppConfiguration'

const stackNavigatorHome = createStackNavigator()

const AppNavigator = () => {
    return <NavigationContainer>
        <stackNavigatorHome.Navigator initialRouteName={'Listing'} screenOptions={{ title: AppName, headerBackTitle:'' }}>
            <stackNavigatorHome.Screen name={'Listing'} component={ListScreen} />
            <stackNavigatorHome.Screen name={'Details'} component={AlbumDetails} />
        </stackNavigatorHome.Navigator>
    </NavigationContainer>
}

export default AppNavigator