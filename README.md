# applesearch

Steps to Run Application.

1. To start clone the app on your system.
2. Navigate to main folder appsearch and do npm install
3. Navigate to ios folder and run 'pod install'.
4. Navigate to main directory and run 'npx react-native run-ios' or 'npx react-native run-android' depending on platform you want to test on.
5. After a successful run you should see the grid listing as shown in screenshots.

![description](screenshots/iOS_1.png)

6. After clicking on any Album artwork you will be navigated to details screen.

![description](screenshots/iOS_2.png)

7. If the Album has a preview video or audio available then 'Play Preview' button will be shown.
8. If the user taps on Play preview button. Application will show play the preview as shown below.

![description](screenshots/iOS_3.png)

9. User can stop preview anytime and go back to details screen.

SCREENSHOTS FROM ANDROID SHOW BELOW

![description](screenshots/Android_1.png)

![description](screenshots/Android_2.png)
